package me.tekkitcommando.cropmodifier.listener;

import de.diddiz.LogBlock.LogBlock;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Random;

public class CropListener implements Listener {

    private Player player = null;
    private Block crop = null;
    private ItemStack item = null;
    private Random rand = new Random();
    private int randLootNum = rand.nextInt(1000) + 1;
    private ArrayList<Material> loot = new ArrayList<Material>();
    private Material randomLoot = null;

    /**
     * Calls the statements and function logic for crop yields when a block is broken.
     * First checks for the specific block types and if not one that
     * was specified it will simply return out of the method.
     *
     * @param event Event that is called when a block is broke.
     */
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        player = event.getPlayer();
        crop = event.getBlock();
        item = player.getEquipment().getItemInMainHand();

        if (crop.getType() == Material.CROPS) {
            if(crop.getData() == 7) {
                event.setCancelled(true);
                dropLogic(Material.WHEAT);
            } else {
                crop.setType(Material.AIR);
                LogBlock.getInstance().getConsumer().queueBlockBreak(player.getName(), crop.getState());
            }
        } else if(crop.getType() == Material.CARROT) {
            if(crop.getData() == 7) {
                event.setCancelled(true);
                dropLogic(Material.CARROT);
            } else {
                crop.setType(Material.AIR);
                LogBlock.getInstance().getConsumer().queueBlockBreak(player.getName(), crop.getState());
            }
        } else if(crop.getType() == Material.NETHER_WARTS) {
            if(crop.getData() == 3) {
                event.setCancelled(true);
                dropLogic(Material.NETHER_WARTS);
            } else {
                crop.setType(Material.AIR);
                LogBlock.getInstance().getConsumer().queueBlockBreak(player.getName(), crop.getState());
            }
        } else {
            return;
        }
    }

    /**
     * Drops the specified item where the block was broken.
     * Then adds 1 durability to the item. Also checks for
     * fortune enchantment and increments the yield amount
     * accordingly as well as gives a chance for random loot.
     *
     * @param mat Material that will be dropped.
     * @param amount Amount that will be dropped.
     */
    private void addDrop(Material mat, int amount) {
        if(item.containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS)) {
            int addedYield = item.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
            crop.setType(Material.AIR);
            crop.getWorld().dropItem(crop.getLocation(), new ItemStack(mat, amount + addedYield));
            // These are just some random loot items that came to my head first.
            loot.add(Material.DIAMOND_SWORD);
            loot.add(Material.DIAMOND);
            loot.add(Material.BEACON);
            randomLoot = loot.get(new Random().nextInt(loot.size()));
            if(randLootNum == 865) {
                crop.getWorld().dropItem(crop.getLocation(), new ItemStack(randomLoot));
            }
            item.setDurability((short) (item.getDurability() + 1));
        } else {
            crop.setType(Material.AIR);
            crop.getWorld().dropItem(crop.getLocation(), new ItemStack(mat, amount));
            item.setDurability((short) (item.getDurability() + 1));
        }
    }

    /**
     * Drop logic based on the type of hoe the player is
     * using. Also checks to see if the Material is wheat
     * if so it will drop a certain amount of seeds based
     * on the hoe that was used.
     *
     * @param mat Material that will be dropped.
     */
    private void dropLogic(Material mat) {
        switch (item.getType()) {
            case WOOD_HOE:
            case STONE_HOE:
                if(crop.getType() == Material.CROPS) {
                    crop.getWorld().dropItem(crop.getLocation(), new ItemStack(Material.SEEDS, 1));
                }
                addDrop(mat, 1);
                LogBlock.getInstance().getConsumer().queueBlockBreak(player.getName(), crop.getState());
                break;
            case GOLD_HOE:
            case IRON_HOE:
                if(crop.getType() == Material.CROPS) {
                    crop.getWorld().dropItem(crop.getLocation(), new ItemStack(Material.SEEDS, 2));
                }
                addDrop(mat, 2);
                LogBlock.getInstance().getConsumer().queueBlockBreak(player.getName(), crop.getState());
                break;
            case DIAMOND_HOE:
                if(crop.getType() == Material.CROPS) {
                    crop.getWorld().dropItem(crop.getLocation(), new ItemStack(Material.SEEDS, 3));
                }
                addDrop(mat, 3);
                LogBlock.getInstance().getConsumer().queueBlockBreak(player.getName(), crop.getState());
                break;
            default:
                crop.setType(Material.AIR);
                LogBlock.getInstance().getConsumer().queueBlockBreak(player.getName(), crop.getState());
                break;
        }
    }
}