package me.tekkitcommando.cropmodifier.listener;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;

import java.util.ArrayList;

public class PistonListener implements Listener {

    private Player player = null;
    private Block block = null;
    private ArrayList<BlockFace> blockFaces = new ArrayList<BlockFace>();

    /**
     * This will prevent from players using pistons to get past the
     * new crop yield system.
     *
     * NOTE: I am not too sure how to make this completely full proof tbh.
     * it will work in some situations though. Maybe if I get a spot on the
     * tean I can pick up something about this from someone. :P
     *
     * @param event Event that is called when a piston is extended.
     */
    @EventHandler
    public void onPistonExtend(BlockPistonExtendEvent event) {
        block = event.getBlock();
        blockFaces.add(BlockFace.EAST);
        blockFaces.add(BlockFace.WEST);
        blockFaces.add(BlockFace.NORTH);
        blockFaces.add(BlockFace.SOUTH);
        blockFaces.add(BlockFace.UP);
        for (BlockFace blockface : blockFaces) {
            if (block.getRelative(blockface).getType() == (Material.CROPS) || block.getRelative(blockface).getType() == Material.CARROT ||
                    block.getRelative(blockface).getType() == Material.NETHER_WARTS || block.getRelative(blockface).getType() == Material.SOIL || block.getRelative(blockface).getType() == Material.SOUL_SAND) {
                event.setCancelled(true);
            }
        }
    }
}
