package me.tekkitcommando.cropmodifier;

import me.tekkitcommando.cropmodifier.listener.CropListener;
import me.tekkitcommando.cropmodifier.listener.PistonListener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public class CropModifier extends JavaPlugin {

    /**
     * A logger for the server.
     */
    private Logger logger;

    /**
     * Functions and statements called when the server is started up.
     */
    @Override
    public void onEnable() {
        logger = getLogger();
        logger.info("Enabled logger!");
        logger.info("Registering events!");
        registerEvents();
        logger.info("Registered events!");
        logger.info("[Crop Modifier] Enabled!");
    }

    /**
     * Functions and statements called when the server is shut down.
     */
    @Override
    public void onDisable() {
        logger.info("[Crop Modifier] Disabled!");
    }

    /**
     * Registers the BlockBreakEvent in the CropListener class.
     */
    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new CropListener(), this);
        getServer().getPluginManager().registerEvents(new PistonListener(), this);
    }
}
